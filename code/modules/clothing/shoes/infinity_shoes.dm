/obj/item/clothing/shoes/antiquated_shoes
	name = "antiquated shoes"
	desc = "Really old, out of fashion shoes"
	icon_state = "antiquated_shoes"
	item_state = "antiquated_shoes"
	icon = 'icons/obj/clothing/infinity_work.dmi'
	worn_icon = 'icons/mob/infinity_work.dmi'
